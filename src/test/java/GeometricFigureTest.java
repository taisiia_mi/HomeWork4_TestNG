import Package3.Circle;
import Package3.Square;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Home on 09.09.2017.
 */

public class GeometricFigureTest {
  private Circle p1 = new Circle(5);
  private  Square p2 = new Square(6);

  @Test
  public void testCircleAreaPos() throws Exception {
    Assert.assertEquals(p1.area(),78.53981633974483,"Positive test");
  }
  @Test
  public void testCircleAreaNeg() throws Exception {
    Assert.assertEquals(p1.area(),78,"Negative test");
  }
  @Test
  public void testSquareAreaPos() throws Exception {
    Assert.assertEquals(p2.area(),36.0,"Positive test");
  }
  @Test
  public void testSquareAreaNeg() throws Exception {
    Assert.assertEquals(p2.area(),30,"Negative test");
  }
  @Test
  public void testCirclePerimeterPos() throws Exception {
    Assert.assertEquals(p1.perimeter(),31.41592653589793,"Positive test");
  }
  @Test
  public void testCirclePerimeterNeg() throws Exception {
    Assert.assertEquals(p1.perimeter(),31,"Negative test");
  }
  @Test
  public void testSquarePerimeterPos() throws Exception {
    Assert.assertEquals(p2.perimeter(),24.0,"Positive test");
  }
  @Test
  public void testSquarePerimeterNeg() throws Exception {
    Assert.assertEquals(p2.perimeter(),30,"Negative test");
  }
}
