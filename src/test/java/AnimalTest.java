import org.testng.annotations.Test;
import package1.Cat;
import org.testng.Assert;
import package1.Dog;

public class AnimalTest {
  private Cat cat = new Cat();
  private Dog dog=new Dog();
@Test
public void testSoundCatPos() throws Exception {
Assert.assertEquals(cat.sound("Mew"),"Mew","Positive Test");
}
@Test
public void testSoundCatNeg() throws Exception {
Assert.assertEquals(cat.sound("Gav"),"Mew","Negative Test");
}
@Test
  public void testSoundDogPos() throws Exception {
    Assert.assertEquals(dog.sound("Gav"),"Gav","Positive Test");
  }
@Test
  public void testSoundDogNeg() throws Exception {
    Assert.assertEquals(dog.sound("Moo"),"Gav","Negative Test");
  }
}
