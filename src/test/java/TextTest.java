import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TextTest {
  @DataProvider(name = "text")
  public Object[][] text() {
  return new Object[][]{
          {"Anna"},
          {"Irina"},
          {"bla-bla-bla"},
          {"more text"},
          {"text-text-text"}
  };
  }

  @Test (dataProvider = "text")
public void getLength(String s){
    int l=s.length();
    Assert.assertTrue(l<10,"it's ok");
  }
  @Test (dataProvider = "text")
  public void getLength2(String s){
    int l=s.length();
    Assert.assertTrue(l>=10,"cut your text");
  }
  }

