package Package3;

/**
 * Created by Home on 25.08.2017.
 */
public class Circle extends GeometricFigure{
  private double radius;

  public Circle(double radius) {
    this.radius = radius;
  }


  @Override
  public double area() {
   double a=Math.PI*radius*radius;
    return a;
  }

  @Override
  public double perimeter() {
    double p=2*Math.PI*radius;
    return p;
  }


}
