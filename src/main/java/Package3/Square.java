package Package3;

/**
 * Created by Home on 25.08.2017.
 */
public class Square extends GeometricFigure {
  private double side;
  public Square(double side) {
    this.side = side;
  }


  @Override
  public double area() {
    double a=side*side;
    return a;

  }

  @Override
  public double perimeter() {
    double p=4*side;
    return p;
  }
}
