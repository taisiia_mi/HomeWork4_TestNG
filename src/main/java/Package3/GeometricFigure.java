package Package3;

/**
 * Created by Home on 25.08.2017.
 */
public abstract class GeometricFigure {
  public abstract double area();
  public abstract double perimeter();
}
