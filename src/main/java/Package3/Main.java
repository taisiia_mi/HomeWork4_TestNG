package Package3;

/**
 * Created by Home on 25.08.2017.
 */
public class Main {
  public static void main(String[] args) {
    GeometricFigure p1 = new Circle (5);
    GeometricFigure p2 = new Square (6);
    System.out.println("The area of Circle: "+p1.area());
    System.out.println("The perimeter of Circle: "+p1.perimeter());
    System.out.println("The area of Square: "+p2.area());
    System.out.println("The perimeter of Square: "+p2.perimeter());
  }
}

